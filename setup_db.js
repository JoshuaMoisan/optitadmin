require("dotenv").config();
const admin = require("firebase-admin");
const serviceAccount = require("./setup_db/firebase-key.json");
const listProducts = require("./setup_db/products.js");
var fs = require("fs");

const allRights = [
  { type: "USER_CREATE", uid: "ceF1bJuT7m0UZlFEdsog" },
  { type: "USER_READ", uid: "NiFkHfhCjzHxefRkYpfa" },
  { type: "USER_UPDATE", uid: "DDEr9o9O26LIlejkVTx1" },
  { type: "USER_DELETE ", uid: "ySNCKIWnpKkml4agR7Fp" },
  { type: "GROUP_CREATE", uid: "N3dAPoESKzeepiwU8CUG" },
  { type: "GROUP_READ ", uid: "EQRxm17ctCUchYdAyaac" },
  { type: "GROUP_UPDATE", uid: "a6v0iPidaNuptB2UziwQ" },
  { type: "GROUP_DELETE", uid: "VbvksEhEshsWfSHzWc4D" },
  { type: "RIGHT_CREATE", uid: "IYa9MT4udcIibw28uHjl" },
  { type: "RIGHT_READ", uid: "PDF3qa6ee9Ioe7XabSa7" },
  { type: "RIGHT_UPDATE", uid: "EjunCc1Yiih6TQCekXMR" },
  { type: "RIGHT_DELETE ", uid: "t7QmuSaBZyGGe4x3D4Ee" },
  { type: "ROLE_CREATE", uid: "dV15ykc2zvgxjpf5BcTj" },
  { type: "ROLE_READ", uid: "KxPREHVRPsI74gO7JPi3" },
  { type: "ROLE_UPDATE", uid: "jP9NJACzFeT9MbhOcU2g" },
  { type: "ROLE_DELETE", uid: "RsU8lG0W0s4xnCucfeet" },
  { type: "JUNCTION_CREATE", uid: "p1vFD2n4EUt4sff25WQN" },
  { type: "JUNCTION_READ", uid: "vkRUx66sFD0dQiVptpBL" },
  { type: "JUNCTION_DELETE", uid: "RW5MrPRLhxx8kNZHWUgD" },

  { type: "PRODUCT_CREATE", uid: "5whTAo8CqJOPTT7zEo91W" },
  { type: "PRODUCT_UPDATE", uid: "gGpz8yzMzrTVicUyhsa0z" },
  { type: "PRODUCT_DELETE", uid: "nwwAbcgniAVvkx3mLaC7l" },
];

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();
const auth = admin.auth();

var buffer = fs.readFileSync("firestore.rules");

listProducts.map((product) => {
  const productsRef = db.collection("products").doc();
  admin
    .storage()
    .bucket(process.env.STORE_BUCKET)
    .upload("./setup_db/" + product.image, {
      destination: "products/" + productsRef.id + ".png",
    });
  productsRef.set({ ...product, id: productsRef.id });
});

// CREATE MARSEILLE EXAMPLE GROUP

const groupRef = db.collection("groups").doc();
groupRef.set({
  name: "Marseille",
  groupId: groupRef.id,
});

// CREATE ADMIN EXAMPLE ROLE

const roleRef = db.collection("roles").doc();
roleRef.set({
  name: "Admin",
  roleId: roleRef.id,
});

// CREATE ADMIN USER

auth
  .createUser({
    email: process.env.ACCOUNT_EMAIL,
    password: process.env.ACCOUNT_PASSWORD,
  })
  .then((user) => {
    console.log(`Created ${JSON.stringify(user)}`);

    // PUSH ADMIN USER IN USERS COLLECTION

    const userRef = db.collection("users").doc(user.uid);

    userRef.set({
      email: user.email,
      uid: user.uid,
      firstname: "adminFirstname",
      name: "adminName",
    });

    // ADD ALL RIGHTS TO ADMIN

    allRights.map((right) => {
      var userrightRef = db
        .collection("users_rights")
        .doc(user.uid + "_" + right.uid);
      userrightRef.set({
        rightId: right.uid,
        userId: user.uid,
      });
    });

    // ADD ADMIN USER TO MARSEILLE GROUP

    const usergroupRef = db
      .collection("users_groups")
      .doc(user.uid + "_" + groupRef.id);
    usergroupRef.set({
      userId: user.uid,
      groupId: groupRef.id,
    });

    // ADD ADMIN USER TO ADMIN GROUP

    const userroleRef = db
      .collection("users_roles")
      .doc(user.uid + "_" + roleRef.id);
    userroleRef.set({
      userId: user.uid,
      roleId: roleRef.id,
    });
  })
  .catch((error) => {
    console.log("Error fetching user data:", error);
  });

// ADD ALL RIGHTS TO RIGHTS COLLECTION

allRights.map((right) => {
  var rightRef = db.collection("rights").doc(right.uid);
  rightRef.set({
    rightId: right.uid,
    type: right.type,
  });
});

admin
  .securityRules()
  .releaseFirestoreRulesetFromSource(buffer)
  .then((msg) => console.log(msg));
