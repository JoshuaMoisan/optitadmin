var baguette = {
  name: "Baguette de pain viennois",
  description: "Description baguette",
  price: 1.9,
  image: "images/baguette_viennois.png",
  calories: 230,
  weight: 70,
  allergen: "Gluten",
  flour: "Blé",
};

var brioche = {
  name: "Brioche",
  description: "Description brioche",
  price: 6,
  image: "images/brioche.png",
  calories: 500,
  weight: 90,
  allergen: "Gluten",
  flour: "Blé",
};

var pain = {
  name: "Le petit pain aux pavots du jardin",
  description:
    "Créé par demande spéciale. Notre Eden Burger Bun joliment garni de graines de lin dorées et de pavot",
  price: 2.8,
  image: "images/pain.png",
  calories: 245,
  weight: 90,
  allergen: "Gluten",
  flour: "Sarasin",
};

var focaccia = {
  name: "Focaccia",
  description: "Description Foccacia",
  price: 2.2,
  image: "images/focaccia.png",
  calories: 250,
  weight: 70,
  allergen: "Gluten",
  flour: "Blé",
};

var muffinNature = {
  name: "Muffin nature",
  description: "Description muffin",
  price: 4.2,
  image: "images/muffin_nature.png",
  calories: 265,
  weight: 45,
  allergen: "Gluten",
  flour: "Blé",
};
var muffinChocolat = {
  name: "Muffin chocolat",
  description: "Description muffin choco",
  price: 4.4,
  image: "images/muffin_chocolat.png",
  calories: 280,
  weight: 50,
  allergen: "Glutten",
  flour: "Blé",
};

var muffinNdc = {
  name: "Muffin noix de coco",
  description: "Description muffin noix de coco",
  price: 4.3,
  image: "images/muffin_ndc.png",
  calories: 275,
  weight: 50,
  allergen: "Glutten",
  flour: "Blé",
};

module.exports = listProducts = [
  baguette,
  brioche,
  pain,
  focaccia,
  muffinNature,
  muffinChocolat,
  muffinNdc,
];
