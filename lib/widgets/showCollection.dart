import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ShowCollection extends StatefulWidget {
  final String collectionAddType;
  final String collectionId;
  final String id1;
  final String id2;
  final String fieldType;
  final bool deleteRight;
  final String collectionType;

  ShowCollection(
      {Key? key,
      required this.collectionAddType,
      required this.collectionId,
      required this.id1,
      required this.id2,
      required this.fieldType,
      required this.deleteRight,
      required this.collectionType})
      : super(key: key);

  @override
  _ShowCollectionState createState() => _ShowCollectionState();
}

class _ShowCollectionState extends State<ShowCollection> {
  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> _stream = FirebaseFirestore.instance
        .collection(widget.collectionAddType)
        .where(widget.id2, isEqualTo: widget.collectionId)
        .snapshots();
    return StreamBuilder<QuerySnapshot>(
      stream: _stream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return Container(
          margin: EdgeInsets.all(4),
          padding: EdgeInsets.all(6),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(snapshot.data!.docs[0][widget.fieldType]),
            SizedBox(
                width: 32,
                height: 32,
                child: ElevatedButton(
                    onPressed: () {
                      if (widget.deleteRight) {
                        debugPrint("Delete junction");
                        FirebaseFirestore.instance
                            .collection(widget.collectionType)
                            .doc(widget.id1 + "_" + widget.collectionId)
                            .delete();
                      }
                    },
                    style: ElevatedButton.styleFrom(
                        primary: widget.deleteRight
                            ? Color(0xFFE94444)
                            : Colors.grey,
                        onPrimary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25))),
                    child: Icon(Icons.close, size: 16)))
          ]),
        );
      },
    );
  }
}
