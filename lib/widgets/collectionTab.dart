import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'showCollection.dart';
import 'popupAdd.dart';

class CollectionTab extends StatefulWidget {
  final String id1;
  final String id2;
  final String fieldType;

  final String tabLabel;
  final String junctionType;
  final String fieldName;
  final String collectionAddType;

  final bool addRight;
  final bool deleteRight;

  CollectionTab({
    Key? key,
    required this.id1,
    required this.id2,
    required this.fieldType,
    required this.tabLabel,
    required this.junctionType,
    required this.fieldName,
    required this.collectionAddType,
    required this.addRight,
    required this.deleteRight,
  }) : super(key: key);
  @override
  _CollectionTabState createState() => _CollectionTabState();
}

class _CollectionTabState extends State<CollectionTab> {
  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance
        .collection(widget.junctionType)
        .where(widget.fieldName, isEqualTo: widget.id1)
        .snapshots();
    return StreamBuilder<QuerySnapshot>(
      stream: _usersStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return new Container(
            decoration: BoxDecoration(
                border: Border.all(
              color: Color(0xFFE94444),
              width: 2.0,
            )),
            padding: EdgeInsets.all(6),
            child: Column(children: [
              Text(widget.tabLabel + 's', style: TextStyle(fontSize: 20)),
              ConstrainedBox(
                  constraints: BoxConstraints(
                      minWidth: 50,
                      maxWidth: 250,
                      maxHeight: MediaQuery.of(context).size.height * 0.5),
                  child: ListView(
                    shrinkWrap: true,
                    children:
                        snapshot.data!.docs.map((DocumentSnapshot document) {
                      return ShowCollection(
                        collectionAddType: widget.collectionAddType,
                        collectionId: (document.data() as dynamic)[widget.id2],
                        id1: (document.data() as dynamic)[widget.fieldName],
                        id2: widget.id2,
                        deleteRight: widget.deleteRight,
                        collectionType: widget.junctionType,
                        fieldType: widget.fieldType,
                      );
                    }).toList(),
                  )),
              ElevatedButton(
                  onPressed: () {
                    if (widget.addRight)
                      showDialog(
                        context: context,
                        builder: (BuildContext context) => PopupAdd(
                            id1: widget.id1,
                            id2: widget.id2,
                            fieldType: widget.fieldType,
                            tabLabel: widget.tabLabel,
                            junctionType: widget.junctionType,
                            addCollectionType: widget.collectionAddType,
                            fieldName: widget.fieldName),
                      );
                  },
                  style: ElevatedButton.styleFrom(
                      primary:
                          widget.addRight ? Color(0xFFE94444) : Colors.grey,
                      onPrimary: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50))),
                  child: Icon(Icons.add))
            ]));
      },
    );
  }
}
