import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

typedef void NameTapCallback(Map<dynamic, dynamic> group);

class CollectionList extends StatelessWidget {
  final NameTapCallback onTap;
  final String collectionType;
  final String title;
  final String subTitle;
  final Icon icon;

  CollectionList(
      {required this.onTap,
      required this.collectionType,
      required this.title,
      required this.subTitle,
      required this.icon});

  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> _stream =
        FirebaseFirestore.instance.collection(collectionType).snapshots();

    return StreamBuilder<QuerySnapshot>(
      stream: _stream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return new ListView(
          children: snapshot.data!.docs.map((DocumentSnapshot document) {
            return new ListTile(
              leading: icon,
              title: new Text((document.data() as dynamic)[this.title]),
              subtitle: new Text((document.data() as dynamic)[this.subTitle]),
              onTap: () => this.onTap((document.data() as dynamic)),
            );
          }).toList(),
        );
      },
    );
  }
}
