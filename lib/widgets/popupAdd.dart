import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PopupAdd extends StatefulWidget {
  final String id1;
  final String id2;
  final String tabLabel;
  final String fieldType;

  final String junctionType;
  final String addCollectionType;
  final String fieldName;

  PopupAdd({
    required this.id1,
    required this.id2,
    required this.tabLabel,
    required this.fieldType,
    required this.junctionType,
    required this.addCollectionType,
    required this.fieldName,
  });
  @override
  _PopupAddState createState() => _PopupAddState();
}

class _PopupAddState extends State<PopupAdd> {
  var selected;

  @override
  Widget build(BuildContext context) {
    return new AlertDialog(
      title: Text('Add ' + widget.tabLabel),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FutureBuilder<QuerySnapshot>(
              future: FirebaseFirestore.instance
                  .collection(widget.addCollectionType)
                  .get(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final List<DocumentSnapshot> documents = snapshot.data!.docs;
                  return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        DropdownButton(
                          onChanged: (value) {
                            setState(() {
                              selected = value.toString();
                            });
                          },
                          value: selected,
                          items: documents.map((value) {
                            return new DropdownMenuItem<String>(
                              value: value[widget.id2],
                              child: new Text(value[widget.fieldType]),
                            );
                          }).toList(),
                          hint: new Text(
                            "Choose " + widget.tabLabel,
                          ),
                        )
                      ]);
                } else if (snapshot.hasError) {
                  return Text('Its Error!');
                } else {
                  return Text('Loading..');
                }
              })
        ],
      ),
      actions: <Widget>[
        new ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Color(0xFFE94444),
              onPrimary: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50))),
          onPressed: () {
            var collectionRef =
                FirebaseFirestore.instance.collection(widget.junctionType);

            collectionRef
                .doc(widget.id1 + "_" + selected)
                .get()
                .then((result) => {
                      if (!result.exists)
                        FirebaseFirestore.instance
                            .collection(widget.junctionType)
                            .doc(widget.id1 + "_" + selected)
                            .set({
                          widget.id2: selected,
                          widget.fieldName: widget.id1
                        }, SetOptions(merge: true))
                    });

            Navigator.of(context).pop();
          },
          child: const Text('Submit'),
        ),
      ],
    );
  }
}
