import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'services/authentication.dart';
import './screens/Home/home.dart';
import './screens/SignIn/signin.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(App());
}

class AuthenticationWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.active) {
          User? user = snapshot.data;
          if (user == null) {
            return SignIn();
          }
          return Home();
        } else {
          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider<Authentication>(
        create: (_) => Authentication(FirebaseAuth.instance),
        child: MaterialApp(
            title: "O'Ptit Admin",
            theme: ThemeData(
              primaryColor: Color(0xFFE94444),
              accentColor: Colors.white,
              scaffoldBackgroundColor: Colors.white,
            ),
            home: AuthenticationWrapper()));
  }
}
