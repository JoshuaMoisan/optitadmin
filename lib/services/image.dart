import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

Future<String> downloadUserImage(id) async {
  String downloadURL = "";
  try {
    downloadURL = await firebase_storage.FirebaseStorage.instance
        .ref('images/' + id + '.png')
        .getDownloadURL();

    debugPrint(downloadURL);
    return (downloadURL);
  } catch (onError) {
    return ("https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png");
  }

  //debugPrint(downloadURL.toString());

  // Within your widgets:
  // Image.network(downloadURL);
}
