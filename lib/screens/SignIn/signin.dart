import '../../services/authentication.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SignIn extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double coef = MediaQuery.of(context).size.width > 800 ? 2 : 1;
    return Scaffold(
        body: Row(
      children: [
        Container(
            width: MediaQuery.of(context).size.width * 0.60,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text('Connexion',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 36 * coef,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFFE94444))),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.25,
                    height: 200,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextField(
                          controller: emailController,
                          decoration: InputDecoration(
                            labelText: 'Email',
                            labelStyle: TextStyle(
                              color: Color(0xFF757575),
                            ),
                            fillColor: Colors.white,
                            filled: true,
                          ),
                        ),
                        TextField(
                          obscureText: true,
                          controller: passwordController,
                          decoration: InputDecoration(
                            labelText: 'Password',
                            labelStyle: TextStyle(
                              color: Color(0xFF757575),
                            ),
                            fillColor: Colors.white,
                            filled: true,
                          ),
                          onEditingComplete: () {
                            context.read<Authentication>().signIn(
                                  email: emailController.text.trim(),
                                  password: passwordController.text.trim(),
                                );
                          },
                        ),
                      ],
                    ),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Color(0xFFE94444),
                        onPrimary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        minimumSize: Size(260, 50)),
                    onPressed: () {
                      context.read<Authentication>().signIn(
                            email: emailController.text.trim(),
                            password: passwordController.text.trim(),
                          );
                    },
                    child: Text("Submit",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.white,
                        )),
                  ),
                ])),
        Container(
          width: MediaQuery.of(context).size.width * 0.40,
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
        )
      ],
    ));
  }
}
