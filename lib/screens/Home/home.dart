import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../services/authentication.dart';
import '../Users/users.dart';
import '../Groups/groups.dart';
import '../Roles/roles.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Home extends StatelessWidget {
  final Stream<DocumentSnapshot> _addJunctionStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_p1vFD2n4EUt4sff25WQN")
      .snapshots();

  final Stream<DocumentSnapshot> _deleteJunctionStream = FirebaseFirestore
      .instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_RW5MrPRLhxx8kNZHWUgD")
      .snapshots();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          title: Text("O'Ptit Dej - Admin Web Console"),
          bottom: TabBar(
            tabs: [
              Text(
                "Users",
                style: TextStyle(fontSize: 22),
              ),
              Text("Groups", style: TextStyle(fontSize: 22)),
              Text("Roles", style: TextStyle(fontSize: 22)),
            ],
          ),
          actions: [
            IconButton(
                onPressed: () {
                  context.read<Authentication>().signOut();
                },
                icon: Icon(Icons.power_settings_new))
          ],
        ),
        body: StreamBuilder<DocumentSnapshot>(
            stream: _addJunctionStream,
            builder: (BuildContext context,
                AsyncSnapshot<DocumentSnapshot> snapshot1) {
              if (snapshot1.hasError) {
                return Text(snapshot1.error.toString());
              }
              if (snapshot1.connectionState == ConnectionState.waiting) {
                return Text("Loading");
              }
              return StreamBuilder<DocumentSnapshot>(
                  stream: _deleteJunctionStream,
                  builder: (BuildContext context,
                      AsyncSnapshot<DocumentSnapshot> snapshot2) {
                    if (snapshot2.hasError) {
                      return Text(snapshot2.error.toString());
                    }
                    if (snapshot2.connectionState == ConnectionState.waiting) {
                      return Text("Loading");
                    }
                    return TabBarView(
                      children: [
                        Users(
                            addJunction: snapshot1.data!.exists,
                            deleteJuction: snapshot2.data!.exists),
                        Groups(
                            addJunction: snapshot1.data!.exists,
                            deleteJuction: snapshot2.data!.exists),
                        Roles(
                            addJunction: snapshot1.data!.exists,
                            deleteJuction: snapshot2.data!.exists),
                      ],
                    );
                  });
            }),
      ),
    );
  }
}
