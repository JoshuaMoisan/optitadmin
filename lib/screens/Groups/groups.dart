import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:optitadmin/widgets/collectionTab.dart';
import './groupDetails.dart';
import './addGroup.dart';
import './editGroup.dart';
import '../../widgets/collectionList.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Groups extends StatefulWidget {
  final addJunction;
  final deleteJuction;

  Groups({Key? key, required this.addJunction, required this.deleteJuction})
      : super(key: key);
  @override
  _GroupsState createState() => _GroupsState();
}

class _GroupsState extends State<Groups> {
  var group = {};

  final Stream<DocumentSnapshot> _addGroupStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_N3dAPoESKzeepiwU8CUG")
      .snapshots();

  final Stream<DocumentSnapshot> _updateGroupStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_a6v0iPidaNuptB2UziwQ")
      .snapshots();

  final Stream<DocumentSnapshot> _deleteGroupStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_VbvksEhEshsWfSHzWc4D")
      .snapshots();

  @override
  Widget build(BuildContext context) {
    bool isScreenWide = MediaQuery.of(context).size.width >= 800;

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.25,
          decoration: BoxDecoration(
              border: Border(
            right: BorderSide(
              color: Color(0xFFE94444),
              width: 3.0,
            ),
          )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 12,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Container(
                    width: MediaQuery.of(context).size.width * 0.13,
                    child: TextField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFE94444)),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10))),
                          hintText: "Search",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFE94444)),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10)))),
                    )),
                StreamBuilder<DocumentSnapshot>(
                    stream: _addGroupStream,
                    builder: (BuildContext context,
                        AsyncSnapshot<DocumentSnapshot> snapshot) {
                      if (snapshot.hasError) {
                        return ElevatedButton(
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                                primary: Colors.grey,
                                onPrimary: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50))),
                            child: Icon(Icons.add));
                      }
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Text("Loading");
                      }

                      return ElevatedButton(
                          onPressed: () {
                            if (snapshot.data!.exists)
                              showDialog(
                                context: context,
                                builder: (BuildContext context) => AddGroup(),
                              );
                          },
                          style: ElevatedButton.styleFrom(
                              primary: snapshot.data!.exists
                                  ? Color(0xFFE94444)
                                  : Colors.grey,
                              onPrimary: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50))),
                          child: Icon(Icons.add));
                    })
              ]),
              SizedBox(
                height: 12,
              ),
              Expanded(
                  child: CollectionList(
                      onTap: (val) {
                        setState(() {
                          this.group = val;
                        });
                      },
                      collectionType: "groups",
                      title: "name",
                      subTitle: "groupId",
                      icon: Icon(Icons.group))),
            ],
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.75,
          child: group['groupId'] != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.05,
                      decoration: BoxDecoration(
                          border: Border(
                        bottom: BorderSide(
                          color: Color(0xFFE94444),
                          width: 3.0,
                        ),
                      )),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                              onPressed: () {
                                debugPrint("Disable");
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: Color(0xFFE94444),
                                  onPrimary: Colors.white,
                                  padding: EdgeInsets.symmetric(horizontal: 50),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50))),
                              child: Text("Disable",
                                  style: TextStyle(fontSize: 20))),
                          StreamBuilder<DocumentSnapshot>(
                              stream: _updateGroupStream,
                              builder: (BuildContext context,
                                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                                if (snapshot.hasError) {
                                  return ElevatedButton(
                                      onPressed: () {},
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.grey,
                                          onPrimary: Colors.white,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 50),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50))),
                                      child: Text("Delete",
                                          style: TextStyle(fontSize: 20)));
                                }
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return Text("Loading");
                                }

                                return ElevatedButton(
                                    onPressed: () {
                                      debugPrint("Edit");
                                      if (snapshot.data!.exists)
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              EditGroup(group: group),
                                        );
                                    },
                                    style: ElevatedButton.styleFrom(
                                        primary: snapshot.data!.exists
                                            ? Color(0xFFE94444)
                                            : Colors.grey,
                                        onPrimary: Colors.white,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50))),
                                    child: Text("Edit",
                                        style: TextStyle(fontSize: 20)));
                              }),
                          StreamBuilder<DocumentSnapshot>(
                              stream: _deleteGroupStream,
                              builder: (BuildContext context,
                                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                                if (snapshot.hasError) {
                                  return Text(snapshot.error.toString());
                                }
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return Text("Loading");
                                }

                                return ElevatedButton(
                                    onPressed: () {
                                      if (snapshot.data!.exists) {
                                        FirebaseFirestore.instance
                                            .collection('groups')
                                            .doc(group['groupId'])
                                            .delete();
                                        FirebaseFirestore.instance
                                            .collection('users_groups')
                                            .where('groupId',
                                                isEqualTo: group['groupId'])
                                            .get()
                                            .then((data) {
                                          for (int i = 0;
                                              i < data.docs.length;
                                              i++) {
                                            FirebaseFirestore.instance
                                                .collection("users_groups")
                                                .doc(data.docs[i].id)
                                                .delete();
                                          }
                                        });
                                        setState(() {
                                          this.group = {};
                                        });
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                        primary: snapshot.data!.exists
                                            ? Color(0xFFE94444)
                                            : Colors.grey,
                                        onPrimary: Colors.white,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50))),
                                    child: Text("Delete",
                                        style: TextStyle(fontSize: 20)));
                              }),
                        ],
                      ),
                    ),
                    GroupDetails(group: group),
                    Flex(
                        direction:
                            isScreenWide ? Axis.horizontal : Axis.vertical,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CollectionTab(
                              id1: group['groupId'],
                              id2: "rightId",
                              tabLabel: "Right",
                              junctionType: "groups_rights",
                              fieldName: "groupId",
                              collectionAddType: "rights",
                              fieldType: "type",
                              addRight: widget.addJunction,
                              deleteRight: widget.deleteJuction),
                          CollectionTab(
                            id1: group['groupId'],
                            id2: "roleId",
                            tabLabel: "Role",
                            junctionType: "groups_roles",
                            fieldName: "groupId",
                            collectionAddType: "roles",
                            fieldType: "name",
                            addRight: widget.addJunction,
                            deleteRight: widget.deleteJuction,
                          ),
                        ])
                  ],
                )
              : Center(child: Text("Please select an group.")),
        )
      ],
    );
  }
}
