import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class RoleDetails extends StatelessWidget {
  final role;

  RoleDetails({required this.role});

  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> _rolesStream = FirebaseFirestore.instance
        .collection('roles')
        .where('roleId', isEqualTo: role['roleId'])
        .snapshots();

    return StreamBuilder<QuerySnapshot>(
        stream: _rolesStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }
          return Row(children: [
            Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(children: [
                        Text('Name : ',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        Text(snapshot.data!.docs[0]['name'],
                            style: TextStyle(fontSize: 18))
                      ]),
                      Row(children: [
                        Text('ID : ',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        Text(snapshot.data!.docs[0]['roleId'],
                            style: TextStyle(fontSize: 18))
                      ]),
                    ]))
          ]);
        });
  }
}
