import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AddRole extends StatelessWidget {
  final TextEditingController nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new AlertDialog(
      title: const Text('Add Role'),
      content: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: 'Name',
                labelStyle: TextStyle(
                  color: Color(0xFF757575),
                ),
                fillColor: Colors.white,
                filled: true,
              ),
            ),
          ]),
      actions: [
        new ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Color(0xFFE94444),
              onPrimary: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50))),
          onPressed: () {
            var doc = FirebaseFirestore.instance.collection('roles').doc();
            doc.set({'roleId': doc.id, 'name': nameController.text.trim()});

            Navigator.of(context).pop();
          },
          child: const Text('Submit'),
        ),
      ],
    );
  }
}
