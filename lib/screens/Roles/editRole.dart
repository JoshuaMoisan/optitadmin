import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class EditRole extends StatelessWidget {
  final role;
  EditRole({required this.role});

  @override
  Widget build(BuildContext context) {
    final TextEditingController nameController =
        TextEditingController(text: role['name']);

    return new AlertDialog(
      title: const Text('Edit Role'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextField(
            controller: nameController,
            decoration: InputDecoration(
              labelText: 'Name',
              labelStyle: TextStyle(
                color: Color(0xFF757575),
              ),
              fillColor: Colors.white,
              filled: true,
            ),
          ),
        ],
      ),
      actions: <Widget>[
        new ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Color(0xFFE94444),
              onPrimary: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50))),
          onPressed: () {
            FirebaseFirestore.instance
                .collection("roles")
                .doc(role['roleId'])
                .update({
              'name': nameController.text.trim(),
            });

            Navigator.of(context).pop();
          },
          child: const Text('Submit'),
        ),
      ],
    );
  }
}
