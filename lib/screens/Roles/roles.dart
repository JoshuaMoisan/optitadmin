import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:optitadmin/widgets/collectionTab.dart';
import './roleDetails.dart';
import './addRole.dart';
import './editRole.dart';
import '../../widgets/collectionList.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Roles extends StatefulWidget {
  final addJunction;
  final deleteJuction;

  Roles({Key? key, required this.addJunction, required this.deleteJuction})
      : super(key: key);
  @override
  _RolesState createState() => _RolesState();
}

class _RolesState extends State<Roles> {
  var role = {};

  final Stream<DocumentSnapshot> _addRoleStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_dV15ykc2zvgxjpf5BcTj")
      .snapshots();

  final Stream<DocumentSnapshot> _updateRoleStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_jP9NJACzFeT9MbhOcU2g")
      .snapshots();

  final Stream<DocumentSnapshot> _deleteRoleStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_RsU8lG0W0s4xnCucfeet")
      .snapshots();

  @override
  Widget build(BuildContext context) {
    bool isScreenWide = MediaQuery.of(context).size.width >= 800;

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.25,
          decoration: BoxDecoration(
              border: Border(
            right: BorderSide(
              color: Color(0xFFE94444),
              width: 3.0,
            ),
          )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 12,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Container(
                    width: MediaQuery.of(context).size.width * 0.13,
                    child: TextField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFE94444)),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10))),
                          hintText: "Search",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFE94444)),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10)))),
                    )),
                StreamBuilder<DocumentSnapshot>(
                    stream: _addRoleStream,
                    builder: (BuildContext context,
                        AsyncSnapshot<DocumentSnapshot> snapshot) {
                      if (snapshot.hasError) {
                        return ElevatedButton(
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                                primary: Colors.grey,
                                onPrimary: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50))),
                            child: Icon(Icons.add));
                      }
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Text("Loading");
                      }

                      return ElevatedButton(
                          onPressed: () {
                            if (snapshot.data!.exists)
                              showDialog(
                                context: context,
                                builder: (BuildContext context) => AddRole(),
                              );
                          },
                          style: ElevatedButton.styleFrom(
                              primary: snapshot.data!.exists
                                  ? Color(0xFFE94444)
                                  : Colors.grey,
                              onPrimary: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50))),
                          child: Icon(Icons.add));
                    })
              ]),
              SizedBox(
                height: 12,
              ),
              Expanded(
                  child: CollectionList(
                      onTap: (val) {
                        setState(() {
                          this.role = val;
                        });
                      },
                      collectionType: "roles",
                      title: "name",
                      subTitle: "roleId",
                      icon: Icon(Icons.attribution))),
            ],
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.75,
          child: role['roleId'] != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.05,
                      decoration: BoxDecoration(
                          border: Border(
                        bottom: BorderSide(
                          color: Color(0xFFE94444),
                          width: 3.0,
                        ),
                      )),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                              onPressed: () {
                                debugPrint("Disable");
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: Color(0xFFE94444),
                                  onPrimary: Colors.white,
                                  padding: EdgeInsets.symmetric(horizontal: 50),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50))),
                              child: Text("Disable",
                                  style: TextStyle(fontSize: 20))),
                          StreamBuilder<DocumentSnapshot>(
                              stream: _updateRoleStream,
                              builder: (BuildContext context,
                                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                                if (snapshot.hasError) {
                                  return ElevatedButton(
                                      onPressed: () {},
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.grey,
                                          onPrimary: Colors.white,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 50),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50))),
                                      child: Text("Delete",
                                          style: TextStyle(fontSize: 20)));
                                }
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return Text("Loading");
                                }

                                return ElevatedButton(
                                    onPressed: () {
                                      debugPrint("Edit");
                                      if (snapshot.data!.exists)
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              EditRole(role: role),
                                        );
                                    },
                                    style: ElevatedButton.styleFrom(
                                        primary: snapshot.data!.exists
                                            ? Color(0xFFE94444)
                                            : Colors.grey,
                                        onPrimary: Colors.white,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50))),
                                    child: Text("Edit",
                                        style: TextStyle(fontSize: 20)));
                              }),
                          StreamBuilder<DocumentSnapshot>(
                              stream: _deleteRoleStream,
                              builder: (BuildContext context,
                                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                                if (snapshot.hasError) {
                                  return Text(snapshot.error.toString());
                                }
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return Text("Loading");
                                }

                                return ElevatedButton(
                                    onPressed: () {
                                      if (snapshot.data!.exists) {
                                        FirebaseFirestore.instance
                                            .collection('roles')
                                            .doc(role['roleId'])
                                            .delete();
                                        FirebaseFirestore.instance
                                            .collection('users_roles')
                                            .where('roleId',
                                                isEqualTo: role['roleId'])
                                            .get()
                                            .then((data) {
                                          for (int i = 0;
                                              i < data.docs.length;
                                              i++) {
                                            FirebaseFirestore.instance
                                                .collection("users_roles")
                                                .doc(data.docs[i].id)
                                                .delete();
                                          }
                                        });
                                        FirebaseFirestore.instance
                                            .collection('groups_roles')
                                            .where('roleId',
                                                isEqualTo: role['roleId'])
                                            .get()
                                            .then((data) {
                                          for (int i = 0;
                                              i < data.docs.length;
                                              i++) {
                                            FirebaseFirestore.instance
                                                .collection("groups_roles")
                                                .doc(data.docs[i].id)
                                                .delete();
                                          }
                                        });
                                        setState(() {
                                          this.role = {};
                                        });
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                        primary: snapshot.data!.exists
                                            ? Color(0xFFE94444)
                                            : Colors.grey,
                                        onPrimary: Colors.white,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50))),
                                    child: Text("Delete",
                                        style: TextStyle(fontSize: 20)));
                              }),
                        ],
                      ),
                    ),
                    RoleDetails(role: role),
                    Flex(
                        direction:
                            isScreenWide ? Axis.horizontal : Axis.vertical,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CollectionTab(
                              id1: role['roleId'],
                              id2: "rightId",
                              tabLabel: "Right",
                              junctionType: "roles_rights",
                              fieldName: "roleId",
                              collectionAddType: "rights",
                              fieldType: "type",
                              addRight: widget.addJunction,
                              deleteRight: widget.deleteJuction),
                        ])
                  ],
                )
              : Center(child: Text("Please select an role.")),
        )
      ],
    );
  }
}
