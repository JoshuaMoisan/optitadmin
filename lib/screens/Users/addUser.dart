import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../services/authentication.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class AddUser extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController firstnameController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  Future<void> setRandomUser() async {
    var url = 'https://randomuser.me/api/?password=upper,lower,12-12?nat=fr';

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      emailController.text = jsonResponse['results'][0]['email'];
      firstnameController.text = jsonResponse['results'][0]['name']['first'];
      nameController.text = jsonResponse['results'][0]['name']['last'];
      passwordController.text = jsonResponse['results'][0]['login']['password'];
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  Widget build(BuildContext context) {
    setRandomUser();

    return new AlertDialog(
      title: const Text('Add User'),
      content: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: emailController,
              decoration: InputDecoration(
                labelText: 'Email',
                labelStyle: TextStyle(
                  color: Color(0xFF757575),
                ),
                fillColor: Colors.white,
                filled: true,
              ),
            ),
            TextField(
              controller: firstnameController,
              decoration: InputDecoration(
                labelText: 'FirstName',
                labelStyle: TextStyle(
                  color: Color(0xFF757575),
                ),
                fillColor: Colors.white,
                filled: true,
              ),
            ),
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: 'Name',
                labelStyle: TextStyle(
                  color: Color(0xFF757575),
                ),
                fillColor: Colors.white,
                filled: true,
              ),
            ),
            TextField(
              controller: passwordController,
              decoration: InputDecoration(
                labelText: 'Password',
                labelStyle: TextStyle(
                  color: Color(0xFF757575),
                ),
                fillColor: Colors.white,
                filled: true,
              ),
              onEditingComplete: () {
                debugPrint("Ojue oue");
              },
            ),
          ]),
      actions: [
        new ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Color(0xFFE94444),
              onPrimary: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50))),
          onPressed: () {
            context.read<Authentication>().signUp(
                  email: emailController.text.trim(),
                  password: passwordController.text.trim(),
                  name: nameController.text.trim(),
                  firstname: firstnameController.text.trim(),
                );
            Navigator.of(context).pop();
          },
          child: const Text('Submit'),
        ),
      ],
    );
  }
}
