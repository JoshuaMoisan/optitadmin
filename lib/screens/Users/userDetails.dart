import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../../services/image.dart';

class UserDetails extends StatelessWidget {
  final user;

  UserDetails({required this.user});

  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance
        .collection('users')
        .where('uid', isEqualTo: user['uid'])
        .snapshots();

    return StreamBuilder<QuerySnapshot>(
        stream: _usersStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }
          return Row(children: [
            Padding(
                padding: EdgeInsets.all(16),
                child: FutureBuilder<String>(
                  future: downloadUserImage(user['uid']),
                  builder:
                      (BuildContext context, AsyncSnapshot<String> snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:
                        return Text(
                          'Image Loading....',
                          style: TextStyle(fontSize: 10),
                        );
                      default:
                        if (snapshot.hasError)
                          return Text('Error: ${snapshot.error}');
                        else
                          return CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 50.0,
                              backgroundImage:
                                  NetworkImage(snapshot.data.toString()));
                    }
                  },
                )),
            Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(children: [
                        Text('Firstname : ',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        Text(snapshot.data!.docs[0]['firstname'],
                            style: TextStyle(fontSize: 18))
                      ]),
                      Row(children: [
                        Text('Name : ',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        Text(snapshot.data!.docs[0]['name'],
                            style: TextStyle(fontSize: 18))
                      ]),
                      Row(children: [
                        Text('Email : ',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        Text(snapshot.data!.docs[0]['email'],
                            style: TextStyle(fontSize: 18))
                      ]),
                      Row(children: [
                        Text('ID : ',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold)),
                        Text(snapshot.data!.docs[0]['uid'],
                            style: TextStyle(fontSize: 18))
                      ])
                    ]))
          ]);
        });
  }
}
