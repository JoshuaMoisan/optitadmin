import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './userDetails.dart';
import './addUser.dart';
import './editUser.dart';
import '../../widgets/collectionTab.dart';
import '../../widgets/collectionList.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Users extends StatefulWidget {
  final addJunction;
  final deleteJuction;

  Users({Key? key, required this.addJunction, required this.deleteJuction})
      : super(key: key);
  @override
  _UsersState createState() => _UsersState();
}

class _UsersState extends State<Users> {
  var user = {};

  final Stream<DocumentSnapshot> _addUserStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_ceF1bJuT7m0UZlFEdsog")
      .snapshots();

  final Stream<DocumentSnapshot> _updateUserStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_DDEr9o9O26LIlejkVTx1")
      .snapshots();

  final Stream<DocumentSnapshot> _deleteUserStream = FirebaseFirestore.instance
      .collection('users_rights')
      .doc(FirebaseAuth.instance.currentUser!.uid + "_ySNCKIWnpKkml4agR7Fp")
      .snapshots();

  @override
  Widget build(BuildContext context) {
    bool isScreenWide = MediaQuery.of(context).size.width >= 800;

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.25,
          decoration: BoxDecoration(
              border: Border(
            right: BorderSide(
              color: Color(0xFFE94444),
              width: 3.0,
            ),
          )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 12,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Container(
                    width: MediaQuery.of(context).size.width * 0.13,
                    child: TextField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFE94444)),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10))),
                          hintText: "Search",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFFE94444)),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10)))),
                    )),
                StreamBuilder<DocumentSnapshot>(
                    stream: _addUserStream,
                    builder: (BuildContext context,
                        AsyncSnapshot<DocumentSnapshot> snapshot) {
                      if (snapshot.hasError) {
                        return ElevatedButton(
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                                primary: Colors.grey,
                                onPrimary: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50))),
                            child: Icon(Icons.add));
                      }
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Text("Loading");
                      }

                      return ElevatedButton(
                          onPressed: () {
                            if (snapshot.data!.exists)
                              showDialog(
                                context: context,
                                builder: (BuildContext context) => AddUser(),
                              );
                          },
                          style: ElevatedButton.styleFrom(
                              primary: snapshot.data!.exists
                                  ? Color(0xFFE94444)
                                  : Colors.grey,
                              onPrimary: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50))),
                          child: Icon(Icons.add));
                    })
              ]),
              SizedBox(
                height: 12,
              ),
              Expanded(
                  child: CollectionList(
                      onTap: (val) {
                        setState(() {
                          this.user = val;
                        });
                      },
                      collectionType: "users",
                      title: "email",
                      subTitle: "uid",
                      icon: Icon(Icons.account_box))),
            ],
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.75,
          child: user['uid'] != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.05,
                      decoration: BoxDecoration(
                          border: Border(
                        bottom: BorderSide(
                          color: Color(0xFFE94444),
                          width: 3.0,
                        ),
                      )),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ElevatedButton(
                              onPressed: () {
                                debugPrint("Disable");
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: Color(0xFFE94444),
                                  onPrimary: Colors.white,
                                  padding: EdgeInsets.symmetric(horizontal: 50),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50))),
                              child: Text("Disable",
                                  style: TextStyle(fontSize: 20))),
                          StreamBuilder<DocumentSnapshot>(
                              stream: _updateUserStream,
                              builder: (BuildContext context,
                                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                                if (snapshot.hasError) {
                                  return ElevatedButton(
                                      onPressed: () {},
                                      style: ElevatedButton.styleFrom(
                                          primary: Colors.grey,
                                          onPrimary: Colors.white,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 50),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(50))),
                                      child: Text("Delete",
                                          style: TextStyle(fontSize: 20)));
                                }
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return Text("Loading");
                                }

                                return ElevatedButton(
                                    onPressed: () {
                                      debugPrint("Edit");
                                      if (snapshot.data!.exists)
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              EditUser(user: user),
                                        );
                                    },
                                    style: ElevatedButton.styleFrom(
                                        primary: snapshot.data!.exists
                                            ? Color(0xFFE94444)
                                            : Colors.grey,
                                        onPrimary: Colors.white,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50))),
                                    child: Text("Edit",
                                        style: TextStyle(fontSize: 20)));
                              }),
                          StreamBuilder<DocumentSnapshot>(
                              stream: _deleteUserStream,
                              builder: (BuildContext context,
                                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                                if (snapshot.hasError) {
                                  return Text(snapshot.error.toString());
                                }
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return Text("Loading");
                                }

                                return ElevatedButton(
                                    onPressed: () {
                                      if (snapshot.data!.exists) {
                                        debugPrint("Delete");
                                        FirebaseFirestore.instance
                                            .collection('users')
                                            .doc(user['uid'])
                                            .delete();
                                        setState(() {
                                          this.user = {};
                                        });
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                        primary: snapshot.data!.exists
                                            ? Color(0xFFE94444)
                                            : Colors.grey,
                                        onPrimary: Colors.white,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(50))),
                                    child: Text("Delete",
                                        style: TextStyle(fontSize: 20)));
                              }),
                        ],
                      ),
                    ),
                    UserDetails(user: user),
                    Flex(
                        direction:
                            isScreenWide ? Axis.horizontal : Axis.vertical,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CollectionTab(
                            id1: user['uid'],
                            id2: "rightId",
                            tabLabel: "Right",
                            junctionType: "users_rights",
                            fieldName: "userId",
                            collectionAddType: "rights",
                            fieldType: "type",
                            addRight: widget.addJunction,
                            deleteRight: widget.deleteJuction,
                          ),
                          CollectionTab(
                            id1: user['uid'],
                            id2: "groupId",
                            tabLabel: "Group",
                            junctionType: "users_groups",
                            fieldName: "userId",
                            collectionAddType: "groups",
                            fieldType: "name",
                            addRight: widget.addJunction,
                            deleteRight: widget.deleteJuction,
                          ),
                          CollectionTab(
                            id1: user['uid'],
                            id2: "roleId",
                            tabLabel: "Role",
                            junctionType: "users_roles",
                            fieldName: "userId",
                            collectionAddType: "roles",
                            fieldType: "name",
                            addRight: widget.addJunction,
                            deleteRight: widget.deleteJuction,
                          ),
                        ])
                  ],
                )
              : Center(child: Text("Please select an user.")),
        )
      ],
    );
  }
}
