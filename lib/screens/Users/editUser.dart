import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class EditUser extends StatelessWidget {
  final user;
  EditUser({required this.user});

  @override
  Widget build(BuildContext context) {
    final TextEditingController firstnameController =
        TextEditingController(text: user['firstname']);
    final TextEditingController nameController =
        TextEditingController(text: user["name"]);
    return new AlertDialog(
      title: const Text('Edit User'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextField(
            controller: firstnameController,
            decoration: InputDecoration(
              labelText: 'Firstname',
              labelStyle: TextStyle(
                color: Color(0xFF757575),
              ),
              fillColor: Colors.white,
              filled: true,
            ),
          ),
          TextField(
            controller: nameController,
            decoration: InputDecoration(
              labelText: 'Name',
              labelStyle: TextStyle(
                color: Color(0xFF757575),
              ),
              fillColor: Colors.white,
              filled: true,
            ),
            onEditingComplete: () {
              debugPrint("Edit User");
            },
          ),
        ],
      ),
      actions: <Widget>[
        new ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Color(0xFFE94444),
              onPrimary: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50))),
          onPressed: () {
            FirebaseFirestore.instance
                .collection("users")
                .doc(user['uid'])
                .update({
              'name': nameController.text.trim(),
              'firstname': firstnameController.text.trim()
            });

            debugPrint(user['uid']);
            debugPrint(nameController.text.trim());
            debugPrint(firstnameController.text.trim());

            Navigator.of(context).pop();
          },
          child: const Text('Submit'),
        ),
      ],
    );
  }
}
