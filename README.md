# O Ptit Admin

Admin Web Console for O Ptit dej app. Flutter advanced module.

## Database installation

Copy your firebase-key.json to setup_db folder

Create .env as follow

```bash
ACCOUNT_EMAIL="admin@demail.com"
ACCOUNT_PASSWORD="admin1234"
STORE_BUCKET="<your-bucket-firebase-storage>"
```

Use the package manager npm to install the required packages

```bash
npm install
```

Run the setup_db script to setup the admin web console database

```bash
node setup_db
```

## Usage

Use flutter run to run the admin web console

```bash
flutter run -d chrome --web-renderer html
```
